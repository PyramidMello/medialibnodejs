var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/medialib');

var trackSchema = new mongoose.Schema({  

  trackNum: Number,
  trackTitle: String,
  trackAlbum: String,
  trackArtist: String,
  trackYear: Number,
  trackGenre: Number,
  trackPath: String
  
});

var Track = mongoose.model('Track', trackSchema);

module.exports.Track = Track;