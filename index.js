var express = require('express'), 
	path = require('path') , 
	request = require('request'),  
    bodyParser = require('body-parser'),
	connect = require('connect'),  
	cheerio = require('cheerio'),   
	app = express();

var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/medialib');

var trackSchema = new mongoose.Schema({  
  _id:String,    
  trackNum: Number,
  trackTitle: String,
  trackAlbum: String,
  trackArtist: String,
  trackYear: Number,
  trackGenre: Number,
  trackPath: String
  
});

var Track = mongoose.model('Track', trackSchema);

	
app.set('views', './views');
app.set('view engine', 'jade');
app.engine("jade", require("jade").__express);
app.use(express.static(path.join(__dirname, 'views')));
app.use(express.static(path.join(__dirname, 'views/files')));
app.use(bodyParser());

app.get('/', function (req, res) {
var contents='<table style="width:100%; margin:auto; color:white"><tr><td>SongArtist</td><td>SongAlbum</td><td>SongTitle</td><td>SongYear</td></tr><br><tr><td><hr></td><td><hr></td><td><hr></td><td><hr></td></tr>';
   
    Track.find({}).sort('trackArtist').exec(function(err, tracks) {
     if (err) {throw err}
         else{
             tracks.forEach(function(item, i, arr) {
                 contents+='<tr style="container">'+'<td>'+item.trackArtist+'</td>'+'<td>'+item.trackAlbum+'</td>'+'<td><a href="/song?id='+item._id+'">'+item.trackTitle+'</a></td>'+'<td>'+item.trackYear+'</td>'+'</tr>';  
                })        
             contents+='<tr><td><hr></td><td><hr></td><td><hr></td><td><hr></td></tr></table>';
             res.render('index', {
	 	     path: path.join(__dirname, 'views'), 
	 	     title: 'Music Home Library',
             content: contents
             });  
         }  
    
        });
    
});

app.get('/song', function (req, res) {
  var id = req.param("id");
  Track.find({_id:'1'}, function(err, tracks) {
  console.log("test load ||"+tracks+"||")   
    });
});

app.listen(1337, function(){
console.log('Express server listening on port 1337');
});