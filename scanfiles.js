console.time('Scan Time'); 
console.log("============================");
console.log("Start Scanning.. ");

var fs = require('fs'),
p = require('path'),
config = require('./config'),
path = config.scanpath;


//рекурсивный обход каталога из path
function recursiveReaddirSync(path) {
  var list = []
    , files = fs.readdirSync(path)
    , stats
    ;
  files.forEach(function (file) {
    stats = fs.lstatSync(p.join(path, file));
    if(stats.isDirectory()) {
      list = list.concat(recursiveReaddirSync(p.join(path, file)));
    } else {
      list.push(p.join(path, file));
    }
  });
  return list;		
}

//костыльная проверка типа файла на медийность
function filesCountFromFolder(el){
	var result = false;
	type=['mp3'];
	for(i=0;i<type.length;i++){
		if(el.substring(el.length - 3)==type[i]) result = true;
	} 
  return result;
}

//сортировка мультимедиа-файлов
function sortMedia(listf) {
  var list = [], i = 0;
	do{
  		if(filesCountFromFolder(listf[i]) == true){		
  			list.push(listf[i]);  			}
  			i++;
		} while(i<listf.length);

   return list;		
}


function getScanList(path,callback){
var resultRead = recursiveReaddirSync(path);
console.log("Files in folder: "+resultRead.length+"");
console.timeEnd('Scan Time');
console.time('Sort Time');  
console.log("Start Sorting Audio Files.. ");
var sortedList = sortMedia(resultRead);
console.timeEnd('Sort Time');
console.log("Contained Media Files: "+sortedList.length);  
callback(sortedList);
}

getScanList(path,function(sortedList){
  
    require('./meta')(sortedList); 

});


